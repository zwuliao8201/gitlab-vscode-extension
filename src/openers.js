const path = require('path');
const vscode = require('vscode');
const gitLabService = require('./gitlab_service');
const {
  getCurrentWorkspaceFolderOrSelectOne,
  getCurrentWorkspaceFolder,
} = require('./services/workspace_service');
const { createGitService } = require('./service_factory');
const { handleError } = require('./log');
const { VS_COMMANDS } = require('./command_names');

const openUrl = url => vscode.commands.executeCommand(VS_COMMANDS.OPEN, vscode.Uri.parse(url));

/**
 * Fetches user and project before opening a link.
 * Link can contain some placeholders which will be replaced by this method
 * with relevant information. Implemented placeholders below.
 *
 * $projectUrl
 * $userId
 *
 * An example link is `$projectUrl/issues?assignee_id=$userId` which will be
 * `gitlab.com/gitlab-org/gitlab-ce/issues?assignee_id=502136`.
 *
 * @param {string} linkTemplate
 */
async function getLink(linkTemplate, workspaceFolder) {
  const user = await gitLabService.fetchCurrentUser();
  const project = await gitLabService.fetchCurrentProject(workspaceFolder);

  return linkTemplate.replace('$userId', user.id).replace('$projectUrl', project.webUrl);
}

async function openLink(linkTemplate, workspaceFolder) {
  await openUrl(await getLink(linkTemplate, workspaceFolder));
}

async function showIssues() {
  const workspaceFolder = await getCurrentWorkspaceFolderOrSelectOne();
  await openLink('$projectUrl/issues?assignee_id=$userId', workspaceFolder);
}

async function showMergeRequests() {
  const workspaceFolder = await getCurrentWorkspaceFolderOrSelectOne();
  await openLink('$projectUrl/merge_requests?assignee_id=$userId', workspaceFolder);
}

async function getActiveFile() {
  const editor = vscode.window.activeTextEditor;
  if (!editor) {
    vscode.window.showInformationMessage('GitLab Workflow: No open file.');
    return undefined;
  }

  const workspaceFolder = await getCurrentWorkspaceFolder();

  if (!workspaceFolder) {
    vscode.window.showInformationMessage('GitLab Workflow: Open file isn’t part of workspace.');
    return undefined;
  }

  let currentProject;
  try {
    currentProject = await gitLabService.fetchCurrentProject(workspaceFolder);
  } catch (e) {
    handleError(e);
    return undefined;
  }

  const gitService = createGitService(workspaceFolder);
  const branchName = await gitService.fetchTrackingBranchName();
  const filePath = path.relative(
    await gitService.getRepositoryRootFolder(),
    editor.document.uri.fsPath,
  );
  const fileUrl = `${currentProject.webUrl}/blob/${branchName}/${filePath}`;
  let anchor = '';

  if (editor.selection) {
    const { start, end } = editor.selection;
    anchor = `#L${start.line + 1}`;

    if (end.line > start.line) {
      anchor += `-${end.line + 1}`;
    }
  }

  return `${fileUrl}${anchor}`;
}

async function openActiveFile() {
  await openUrl(await getActiveFile());
}

async function copyLinkToActiveFile() {
  const fileUrl = await getActiveFile();

  if (fileUrl) {
    await vscode.env.clipboard.writeText(fileUrl);
  }
}

async function openCurrentMergeRequest() {
  const workspaceFolder = await getCurrentWorkspaceFolderOrSelectOne();
  const mr = await gitLabService.fetchOpenMergeRequestForCurrentBranch(workspaceFolder);

  if (mr) {
    await openUrl(mr.web_url);
  }
}

async function openCreateNewIssue() {
  const workspaceFolder = await getCurrentWorkspaceFolderOrSelectOne();
  openLink('$projectUrl/issues/new', workspaceFolder);
}

async function openCreateNewMr() {
  const workspaceFolder = await getCurrentWorkspaceFolderOrSelectOne();
  const project = await gitLabService.fetchCurrentProject(workspaceFolder);
  const branchName = await createGitService(workspaceFolder).fetchTrackingBranchName();

  openUrl(`${project.webUrl}/merge_requests/new?merge_request%5Bsource_branch%5D=${branchName}`);
}

async function openProjectPage() {
  const workspaceFolder = await getCurrentWorkspaceFolderOrSelectOne();
  openLink('$projectUrl', workspaceFolder);
}

async function openCurrentPipeline(workspaceFolder) {
  const { pipeline } = await gitLabService.fetchPipelineAndMrForCurrentBranch(workspaceFolder);

  if (pipeline) {
    openUrl(pipeline.web_url);
  }
}

async function compareCurrentBranch() {
  let project = null;
  let lastCommitId = null;
  const workspaceFolder = await getCurrentWorkspaceFolderOrSelectOne();

  project = await gitLabService.fetchCurrentProject(workspaceFolder);
  lastCommitId = await createGitService(workspaceFolder).fetchLastCommitId();

  if (project && lastCommitId) {
    openUrl(`${project.webUrl}/compare/master...${lastCommitId}`);
  }
}

exports.openUrl = openUrl;
exports.showIssues = showIssues;
exports.showMergeRequests = showMergeRequests;
exports.openActiveFile = openActiveFile;
exports.copyLinkToActiveFile = copyLinkToActiveFile;
exports.openCurrentMergeRequest = openCurrentMergeRequest;
exports.openCreateNewIssue = openCreateNewIssue;
exports.openCreateNewMr = openCreateNewMr;
exports.openProjectPage = openProjectPage;
exports.openCurrentPipeline = openCurrentPipeline;
exports.compareCurrentBranch = compareCurrentBranch;
